package ru.tsc.karbainova.tm.service;

import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.api.service.ITaskService;
import ru.tsc.karbainova.tm.enumerated.Status;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyIndexException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(String userId, String name) {
        if (userId == null || userId.isEmpty()) return;
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
    }

    @Override
    public void create(String userId, String name, String description) {
        if (name == null || name.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @Override
    public void add(String userId, Task task) {
        if (userId == null || userId.isEmpty()) return;
        if (task == null) throw new TaskNotFoundException();
        taskRepository.add(userId, task);
    }

    @Override
    public void remove(String userId, Task task) {
        if (task == null) throw new TaskNotFoundException();
        taskRepository.remove(userId, task);
    }

    @Override
    public List<Task> findAll(String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return taskRepository.findAll();
    }

    @Override
    public Task updateById(String userId, String id, String name, String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findById(userId, id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(String userId, Integer index, String name, String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByIndex(userId, index);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task removeByIndex(String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.removeByIndex(userId, index);
    }

    @Override
    public Task removeByName(String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeByName(userId, name);
    }

    @Override
    public Task findByIndex(String userId, Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        return taskRepository.findByIndex(userId, index);
    }

    @Override
    public Task findByName(String userId, String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    public Task startById(String userId, String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(String userId, Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = taskRepository.findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(String userId, String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishById(String userId, String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = taskRepository.findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishByIndex(String userId, Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        final Task task = taskRepository.findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishByName(String userId, String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = taskRepository.findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETE);
        return task;
    }
}
