package ru.tsc.karbainova.tm.service;

import java.util.Comparator;
import java.util.List;
import ru.tsc.karbainova.tm.api.repository.IRepository;
import ru.tsc.karbainova.tm.api.service.IService;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.model.AbstractEntity;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {
    protected IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public List<E> findAll(String userId, Comparator<E> comparator) {
        return repository.findAll(userId, comparator);
    }

    @Override
    public E add(E entity) {
        return repository.add(entity);
    }

    @Override
    public E findById(String userId, String id) {
        return repository.findById(userId, id);
    }

    @Override
    public E removeById(String userId, String id) {
        return repository.removeById(userId, id);
    }

    @Override
    public void remove(E entity) {
        repository.remove(entity);
    }

    @Override
    public void clear(String userId) {
        repository.clear(userId);
    }
}
