package ru.tsc.karbainova.tm.api.repository;

import ru.tsc.karbainova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void add(String userId, Project project);

    void remove(String userId, Project project);

    boolean existsById(String userId, String id);

    List<Project> findAll(String userId);

    List<Project> findAll(String userId, Comparator<Project> comparator);

    void clear(String userId);

    Project findById(String userId, String id);

    Project findByIndex(String userId, int index);

    Project findByName(String userId, String name);

    Project removeById(String userId, String id);

    Project removeByName(String userId, String name);

    Project removeByIndex(String userId, int index);


}
