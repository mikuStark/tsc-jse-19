package ru.tsc.karbainova.tm.api.repository;

import ru.tsc.karbainova.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    List<E> findAll(String userId, Comparator<E> comparator);

    E add(E entity);

    E findById(String userId, String id);

    E removeById(String userId, String id);

    void remove(E entity);

    void clear(String userId);
}
