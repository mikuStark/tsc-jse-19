package ru.tsc.karbainova.tm.command.serv;

import ru.tsc.karbainova.tm.command.AbstractCommand;

public class ShowArgumentsCommand extends AbstractCommand {
    @Override
    public String name() {
        return "arguments";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "All arguments";
    }

    @Override
    public void execute() {
        for (final AbstractCommand command : serviceLocator.getCommandService().getArguments()) {
            System.out.println(command.toString());
        }
    }
}
